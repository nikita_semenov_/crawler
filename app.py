import asyncio
import uvloop

from utils.graph import Graph
from utils.crawler import Crawler


async def main():
    graph = Graph()
    defaultCrawler = Crawler(loop=loop)
    graph = await defaultCrawler.crawl_with_depth(
        root_url="https://www.python.org", depth_limit=2, graph=Graph()
    )
    graph.export_to_file()


loop = uvloop.new_event_loop()
asyncio.set_event_loop(loop)
loop.run_until_complete(main())
loop.close()
