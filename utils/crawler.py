from aiohttp import ClientSession
import asyncio
import async_timeout
from urllib.parse import urljoin, urldefrag
from concurrent.futures._base import CancelledError


class Crawler(object):
    def __init__(self, get_urls_from_page=None, loop=None):
        if get_urls_from_page is None:
            self.get_urls_from_page = get_urls_from_page_default

        if loop is None:
            self._loop = asyncio.get_event_loop()
        else:
            self._loop = loop

        self._queue_to_fetch_urls = asyncio.Queue(maxsize=100, loop=self._loop)
        self._queue_to_update_graph = asyncio.Queue(maxsize=100, loop=self._loop)
        self._queue_to_manage_main_process = asyncio.Queue(maxsize=100, loop=self._loop)
        self._limit_http_requests = asyncio.Semaphore(15, loop=self._loop)

    async def _fetch_page(self, session, url):
        try:
            async with self._limit_http_requests:
                with async_timeout.timeout(10):
                    async with session.get(url) as response:
                        if response.status == 200:
                            html = await response.text()
                            return {"error": "", "html": html}
                        else:
                            return {"error": response.status, "html": ""}
        except Exception as err:
            return {"error": err, "html": ""}

    async def _crawl_page(self, session, home_url, url_depth):
        try:
            resp = await self._fetch_page(session, home_url)

            if not resp["error"]:
                new_urls = self.get_urls_from_page(resp, home_url)
                for new_url in new_urls:
                    await self._queue_to_update_graph.put(
                        (home_url, new_url, url_depth)
                    )

            await self._queue_to_manage_main_process.put(-1)
        except Exception as err:
            print("crawl_page Error: {}".format(err))

    async def _fetch_urls(self):
        try:
            async with ClientSession() as session:
                while True:
                    url_to_fetch, url_depth = await self._queue_to_fetch_urls.get()
                    self._loop.create_task(
                        self._crawl_page(session, url_to_fetch, url_depth)
                    )
                    self._queue_to_fetch_urls.task_done()
        except CancelledError:
            pass
        except GeneratorExit:
            pass
        except Exception as err:
            print("fetch_urls Error: {}".format(err))

    async def _update_graph(self, graph, depth_limit):
        try:
            while True:
                from_url, to_url, url_depth = await self._queue_to_update_graph.get()

                graph.add_vertex(from_url)
                graph.add_edge(from_url, to_url)

                if url_depth < depth_limit:
                    if to_url not in graph.vertices:
                        await self._queue_to_fetch_urls.put((to_url, url_depth + 1))
                        await self._queue_to_manage_main_process.put(1)

                self._queue_to_update_graph.task_done()
        except CancelledError:
            pass
        except GeneratorExit:
            pass
        except Exception as err:
            print("update_graph Error: {}".format(err))

    async def crawl_with_depth(
        self, root_url="https://www.python.org", depth_limit=2, graph=None
    ):
        try:
            task_to_update_graph = self._loop.create_task(
                self._update_graph(graph, depth_limit)
            )
            task_to_fetch_urls = self._loop.create_task(self._fetch_urls())
            await self._queue_to_fetch_urls.put((root_url, 1))
            node_in_progress = 1
            print("start crawl")

            while True:
                node_inc = await self._queue_to_manage_main_process.get()
                node_in_progress += node_inc
                if (
                    node_in_progress == 0
                    and self._queue_to_update_graph.empty()
                    and self._queue_to_fetch_urls.empty()
                ):
                    self._queue_to_manage_main_process.task_done()
                    task_to_update_graph.cancel()
                    task_to_fetch_urls.cancel()
                    break

            print("done crawl")
            return graph
        except CancelledError:
            pass
        except GeneratorExit:
            pass
        except Exception as err:
            print("crawl_with_depth canceled or error: {}".format(err))


def get_urls_from_page_default(html, home_url):
    new_urls = [
        url.split('"')[0] for url in str(html).replace("'", '"').split('a href="')[1:]
    ]
    return set([urljoin(home_url, urldefrag(new_url)[0]) for new_url in new_urls])
