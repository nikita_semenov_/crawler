class Graph(object):
    def __init__(self, graph_dict=None):
        if graph_dict == None:
            graph_dict = {}
        self.__graph_dict = graph_dict

    @property
    def vertices(self):
        return self.__graph_dict.keys()

    def add_vertex(self, vertex):
        if vertex not in self.__graph_dict:
            self.__graph_dict[vertex] = set()

    def add_edge(self, from_v, to_v):
        if from_v in self.__graph_dict:
            self.__graph_dict[from_v].add(to_v)
        else:
            self.__graph_dict[from_v] = {to_v}

    def export_to_file(self, filename="./graph.txt"):
        with open(filename, "w") as f:
            f.write(repr(self.__graph_dict))
